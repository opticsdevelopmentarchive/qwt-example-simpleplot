#-------------------------------------------------
#
# Project created by QtCreator 2013-07-09T16:03:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qwt-example-simpleplot
TEMPLATE = app


SOURCES += main.cpp\
#        mainwindow.cpp

#HEADERS  += mainwindow.h

#FORMS    += mainwindow.ui

INCLUDEPATH += /usr/include/qwt
LIBS += -lqwt
